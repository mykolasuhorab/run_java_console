## Run Java Console

1. Create .txt file and rename it to .java
2. Open and past Java main method with print function
3. Save
4. Open cmd on the same path and run 
> javac Jjava.java ```will create binary Jjava.class```
5. Run 
> java -classpath . Jjava ```print: Hello```
6. Add txt file with name Manifest.mf
7. Open Manifest.mf and put  ```Manifest-Version: 1.0  Main-Class: Jjava  Created-By: 10.0.2 (Oracle Corporation)```
8. Save and after run 
> jar cvfm Jjava.jar Manifest.mf *.class ```will create Jjava.jar```
9. Run 
> java -jar Jjava.jar ```print: Hello```